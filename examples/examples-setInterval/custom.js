/**
 * @function deNull函数
 * @description 去掉Null/Undefined的情况
 */
function deNull(str) {
    str = typeof str == 'undefined' || str == null || typeof str == 'null' ?
        '' :
        str;
    return str;
}

/**
 * @function 查询URL参数
 */
function getQueryString(key) {
    var reg = new RegExp('(^|&)' + key + '=([^&]*)(&|$)');
    var result = window.location.search.substr(1).match(reg);
    return result ? decodeURIComponent(result[2]) : null;
}

/**
 * @function 日期格式化函数
 */
function Dateformat(fthis, fmt) {
    var o = {
        'M+': fthis.getMonth() + 1, //月份
        'd+': fthis.getDate(), //日
        'h+': fthis.getHours(), //小时
        'm+': fthis.getMinutes(), //分
        's+': fthis.getSeconds(), //秒
        'q+': Math.floor((fthis.getMonth() + 3) / 3), //季度
        S: fthis.getMilliseconds(), //毫秒
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(
            RegExp.$1,
            (fthis.getFullYear() + '').substr(4 - RegExp.$1.length)
        );
    }
    for (var k in o) {
        if (new RegExp('(' + k + ')').test(fmt)) {
            fmt = fmt.replace(
                RegExp.$1,
                RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length)
            );
        }
    }
    return fmt;
}

$(function() {
    //获取用户代码
    var userID = deNull(getQueryString('userID'));
    //获取水印内容
    var showMarkText = '蓝光BRC:{userID}   '.replace('{userID}', userID);
    //设置水印内容
    var showText = showMarkText + Dateformat(new Date(), 'yyyy/MM/dd hh:mm:ss');

    //初始化水印操作
    watermark.init({
        watermark_txt: showText,
        watermark_width: 200,
    });

    //每3秒刷新一次  3000的单位是毫秒
    setInterval(function() {
        var showText1 =
            showMarkText + Dateformat(new Date(), 'yyyy/MM/dd hh:mm:ss');
        watermark.load({
            watermark_txt: showText1,
            watermark_width: 200,
        });
    }, 3000);
});